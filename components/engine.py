import re
from matrix_client.client import User
from datetime import datetime
from PIL import Image
import urllib.request
import io


class Engine():
    """
    Creates engine object and proceed essential methods
    """
    category = ''
    text = ''
    room = None
    sender_id = ''
    chat_id = ''
    client = None
    user = None


    def __init__(self, client):
        self.client = client

    # check on every request. Returns text and category if exists
    def prepare_incoming_data(self, room, event):
        self.text = event['content']['body']
        self.room = room
        self.chat_id = event['room_id']
        self.sender_id = event['sender']
        self.user = User(self.client.api, self.sender_id)
        self.username = self.user.get_display_name() 

    def send(self, message):
        """
        Send proceeded message
        """

        self.room.send_html(message)
    
    def send_text(self, room, message):
        room.send_html(message)

    def send_picture(self, room, mxc):
        title = "{0}.jpeg".format(datetime.now().strftime("%H:%M:%S %d/%m/%Y"))
        room.send_image(mxc, title)

    def upload_picture_and_return_mxc(self, URL):
        with urllib.request.urlopen(URL) as url:
            f = io.BytesIO(url.read())
        
        return self.client.api.media_upload(f, "image/jpeg")
