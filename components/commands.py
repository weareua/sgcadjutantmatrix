import random
from components.lingua import greetings_list, thx_list, codex_list
from components.meaning_search import get_meaning


def start(bot, engine):
    start_text = "Мої вітання, {0}. ".format(
        update.message.from_user.first_name) + "\n" + \
        "Головно, я є частиною Малої Галактичної Ради." + \
        " Можете спробувати долучитися за посиланням:" + \
        " https://t.me/joinchat/EO9Rg0GzT8IAm3Uh-gltSA" + "\n" + \
    engine.send(start_text)


def hi(engine):
    
    attr_list = [
        random.choice(greetings_list), engine.username]
    random.shuffle(attr_list)
    greetings_text = "{0}, {1}!".format(attr_list[0], attr_list[1])
    if not greetings_text[0].isupper():
        greetings_text = greetings_text[:1].capitalize() + greetings_text[1:]

    engine.send(greetings_text)


def thx(engine):
    attr_list = [
        random.choice(thx_list), engine.username]
    random.shuffle(attr_list)
    thx_text = "{0}, {1}.".format(attr_list[0], attr_list[1])
    if not thx_text[0].isupper():
        thx_text = thx_text[:1].capitalize() + thx_text[1:]

    engine.send(thx_text)

def optional_talks(engine):
    """
    Make sure that bot talks not every time
    """
    quess_list = range(2)
    quess = random.choice(quess_list)
    if quess in [1,2]:
        thx(engine)


def meaning(engine, actual_query, lang):
    err_text = 'На жаль, мені ' + \
        'не вдалося знайти жодного запису з цього приводу.'
    try:
        text = get_meaning(actual_query, lang)
        if not text:
            text = err_text
        engine.send(text)
    except Exception:
        # TODO: handle it in a proper way
        engine.send(err_text)

def whole_codex(engine):
    text_response = (('<br>' * 2).join(codex_list))
    engine.send(text_response)


def rule_codex(engine, actual_rule_index):
    actual_rule_index = int(actual_rule_index)
    if actual_rule_index < 1 or actual_rule_index > 20:
        text_response = '{0}, Кодекс Малої Галактичної Ради налічує лише 20 пунктів.'.format(engine.username) # noqa
    else:
        text_response = codex_list[actual_rule_index]
    engine.send(text_response)
